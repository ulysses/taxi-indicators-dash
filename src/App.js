
//import logo from './logo.svg';
//import './App.css';
import React, { Component } from 'react';
import {ResponsiveLine} from "nivo"
//import my_data from './data/sampleTest.json'
import my_data from './data/csvjson.json';
//import my_data from './data/test.json';
//import my_data from './data/nivoTest.json';
//import {XYPlot, XAxis, YAxis, HorizontalGridLines, LineSeries, VerticalGridLines, LineMarkSeries} from 'react-vis';
import '../node_modules/react-vis/dist/style.css';
import Graph from './components/Graph'
import Selector from './components/Selector'
//import Slider from './components/Slider'
import DateInput from './components/DateInput'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
     data:[],
     graphColors : {
        yellow: "	#FFA500",
        green:"	#006400",
        bc: "#000000",
        hv: "#FF1493",
        lx: "#000080",
        lv: "	#FF0000"

     },
     selectChoices: {
        tpd: "Trips Per Day",
        ud: "Unique Drivers",
        uv: "Unique Vehicles"
     },
     selectInput: "Trips Per Day",
     dateInput: "2018-06-01"
  }
  }

  componentDidMount() {  
    my_data.map(item => {item["Month/Year"] = item["Month/Year"] + '-01'; return Date.parse(item)})
    my_data.map(item => {item["Trips Per Day"] = item["Trips Per Day"].replace(",",""); return item})
    my_data.map(item => {item["Unique Drivers"] = item["Unique Drivers"].replace(",",""); return item})
    my_data.map(item => {item["Unique Vehicles"] = item["Unique Vehicles"].replace(",",""); return item}) 
    my_data.sort( this.compare ); 
    this.setState({data: my_data})
    this.processGraph(my_data, this.state.selectInput)
  }


  compare = ( a, b ) => {
    if ( a["Month/Year"] < b["Month/Year"] ){
      return -1;
    }
    if ( a["Month/Year"] > b["Month/Year"]){
      return 1;
    }
    return 0;
  }

  processGraph = (data_set, variable) => {

    // track our inputs
    console.log(this.state.dateInput)
    console.log(this.state.selectInput)
    console.log("state data", this.state.data)
    //filter data by date
    let result = data_set;

    result = Object.values(result.reduce((r, o) => {
    var name = o["Month/Year"];
  //var group = o["group one"];
    r[name] = r[name] || { name };    
    r[name][{ "Green": 'Green',
            "Yellow": 'Yellow',
            "FHV - High Volume": 'HighVolume',
            "FHV - Black Car": "BlackCar",
            "FHV - Livery": "Livery",
            "FHV - Lux Limo": "LuxLimo"
          }[o[["License Class"]]]] = o[variable];
     return r;
    }, {}));
    console.log("final", result);

    //final result is sent to the data
    this.setState({data : result});
  }

  handleSelect = (e) => {
    console.log(e.target.value);
    this.setState({selectInput: e.target.value}) // update state with new filtered variable
    this.updateData();
    //this.processGraph(this.state.selectInput)
    //this.processGraph(e.target.value)
  }

  handleSlider = (e) => {
    console.log(e.target.value);
    this.setState({sliderInput:e.target.value}) //update state with the new slider values
    //this.updateData();
  }

  handleDateInput = (e) => {
    console.log(e.target.value);
    this.setState({dateInput:e.target.value}) //update state with the new date value
    //this.updateData();
    //this.processGraph(e.target.value)
  }

  updateData = () => {
    //console.log(this.state.data);
    this.processGraph(my_data, this.state.selectInput);
  }

  textStyles = {
    textAlign: "center"
  }

  backgroundStyles = {
    backgroundImage: "url('https://cdn1.vectorstock.com/i/1000x1000/01/20/taxi-city-background-vector-2140120.jpg')",
    backgroundPosition: "5px -160px"
  }

  render() {
    return (
      <div style={this.backgroundStyles}>
        <h2 style={this.textStyles}>Quick Dash</h2>
        <p style={this.textStyles}>
          <b>
            Quick Dash was built as a practice app for data visualization in react, enjoy . . . 
          </b>
        </p>
        <p style={this.textStyles}><b>Choose your Metric</b></p>
       <Selector 
          selectChoices = {this.state.selectChoices}
          handleSelect = {this.handleSelect}/>
        
        <p style={this.textStyles}><b>Choose a Start Date</b></p>
       <DateInput handleDateInput = {this.handleDateInput} />
        <br/>
        <br/>
        <h2 style = {{textAlign:"center",
                      //marginLeft:"25%",
                      //marginRight: "55%",
                      color:"white",
                      outlineColor:"blue"}}
        >
        {`${this.state.selectInput} from ${this.state.dateInput} Onward`}
        </h2>
       <Graph data = {this.state.data.filter(d => {return (d['name'] >= this.state.dateInput);})} 
              graphColors={this.state.graphColors}/>

      </div>
    );
  }
}

export default App;



/**
 * 
 *  <button onChange={this.checkData}>click me</button>
        <XYPlot width={400} height={300}><XAxis/><YAxis/>
          <HorizontalGridLines />
          <VerticalGridLines />
          <LineMarkSeries data={this.state.data} />
        </XYPlot>
 * 
 * 
 * 
 *  checkData = () => {
  //  my_data.map(e=> {
  //    console.log(this.state.secondData)
  //  })
  this.state.secondData.map(e=> {
    console.log(e)
  })
  }

  pushData = () => {
    my_data.map(e=> {
      this.state.secondData.push.apply(this.state.secondData, e['Month/Year'])
    })
  }
 *    <button onClick ={this.checkData}>check data</button>
        <button onClick={this.pushData}>push_data</button>
  const proxyurl = "https://cors-anywhere.herokuapp.com/";
    const url = "https://www1.nyc.gov/assets/tlc/downloads/csv/data_reports_monthly_indicators.csv"; // site that doesn’t send Access-Control-*
    const request = async () => {
      await fetch(proxyurl + url)
      .then(res=> res.json())
      .then(res=> console.log(res)
         // this.setState({data:res})
      );
    }
    request();    


var newData = [];
var data = [{
    "Month/Year": "2019-09",
    "group": "blue",
    "flights": "668620"
  },
  {
    "Month/Year": "2019-10",
    "group": "blue",
    "flights": "662520"
  },
  {
    "Month/Year": "2019-09",
    "group": "green",
    "flights": "662520"
  },
  {
    "Month/Year": "2019-10",
    "group": "green",
    "flights": "678520"
  }
];

data.forEach((item, index) => {
    let temp = {};
    let exists = newData.find(e => e.name === item['Month/Year']);
    if (exists) { // if the date exists in the array already
	if (item.group === 'green') {
            exists.gr = item.flights;
        } else {
    	    exists.hv = item.flights;
	}
    } else {
        temp.name = item['Month/Year'];
        if (item.group === 'green') {
            temp.gr = item.flights;
        } else {
    	    temp.hv = item.flights;
	}
        newData.push(temp);
    }
});

console.log(newData);
.as-console-wrapper { max-height: 100% !important; top: 0; }


let result = [];
    result = Object.values(my_data.reduce((r, o) => {
      var name = o["Month/Year"];
      //var green = o["Green"]
      //var blue = o["Yellow"]
      var flights = o["Trips Per Day"]
      var group = o["License Class"]
      r[name] = r[name] || { name };
      r[name][{ green: 'Green', blue: 'Yellow'}[o.group]] = o.flights;
      console.log(r)
      return r;



let result = [];
    result = Object.values(my_data.reduce((r, o) => {
        var name = o["Month/Year"];
        var group = o["License Class"];
        var flights = o["Trips Per Day"];
        r[name] = r[name] || { name };
        r[name][{ Green: 'gr', Yellow: 'hv'}[o.group]] = o.flights;
        return r;
    }, {}));


    //write spolution

var data = [{ "Month/Year": "2019-09", "group one": "blue", flights: "668620" },
                { "Month/Year": "2019-10", "group one": "blue", flights: "662520" },
                { "Month/Year": "2019-09", "group one": "green one", flights: "662520" },
                { "Month/Year": "2019-10", "group one": "green one", flights: "678520" },
                { "Month/Year": "2019-10", "group one": "red", flights: "678520" }
              
              ],
    result = Object.values(data.reduce((r, o) => {
        var name = o["Month/Year"];
        //var group = o["group one"];
        r[name] = r[name] || { name };
        r[name][{ "green one": 'gr', blue: 'hv'}[o[["group one"]]]] = o.flights;
        return r;
    }, {}));


    var data = [{ "Month/Year": "2019-09", "group one": "blue", "Trips Per Day": "668620" },
                { "Month/Year": "2019-10", "group one": "blue", "Trips Per Day": "662520" },
                { "Month/Year": "2019-09", "group one": "green one", "Trips Per Day": "662520" },
                { "Month/Year": "2019-10", "group one": "green one", "Trips Per Day": "678520" },
                { "Month/Year": "2019-10", "group one": "red", "Trips Per Day": "678520" }
              
              ],
    result = Object.values(data.reduce((r, o) => {
        var name = o["Month/Year"];
        //var group = o["group one"];
        r[name] = r[name] || { name };
        r[name][{ "green one": 'gr', blue: 'hv'}[o[["group one"]]]] = o["Trips Per Day"];
        return r;
    }, {}));
    


    ///////////

    let result = [];
    result = Object.values(my_data.reduce((r, o) => {
      var name = o["Month/Year"];
      //var group = o["group one"];
      r[name] = r[name] || { name };
      r[name][{ "Yellow": 'gr', "Green": 'hv'}[o[["License Class"]]]] = o["Trips Per Day"];
      return r;
  }, {}));

////////////////////

<LineChart
        width={800}
        height={600}
        data={this.state.data}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="Green" stroke={this.state.graphColors.green} activeDot={{ r: 8 }} />
        <Line type="monotone" dataKey="Yellow" stroke={this.state.graphColors.yellow} />
        <Line type="monotone" dataKey="HighVolume" stroke={this.state.graphColors.hv} />
        <Line type="monotone" dataKey="LuxLimo" stroke={this.state.graphColors.lx} />
        <Line type="monotone" dataKey="Livery" stroke={this.state.graphColors.lv}/>
        <Line type="monotone" dataKey="BlackCar" stroke={this.state.graphColors.bc} />

      </LineChart>
*/












