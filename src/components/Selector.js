import React from 'react';
import classes from './selector.module.css'

const selector = ( props ) => {

    return (

            <select 
                onChange={props.handleSelect} 
                className={classes.selector} >
                <option>{props.selectChoices.tpd}</option>
                <option>{props.selectChoices.ud}</option>
                <option>{props.selectChoices.uv}</option>
            </select>
 
    )
};

export default selector;