import React, { Component } from 'react';
import {
    LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer
  } from 'recharts';
 
  
  
  const graph = ( props ) => {

      return (
          <div style={{backgroundColor: "#FAFAD2"}}
          >
          <ResponsiveContainer width = "100%" height={450} >
            <LineChart 
                data={props.data}
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                <CartesianGrid strokeDasharray="4 4" />
                <XAxis dataKey="name" angle={0} textAnchor="end" tick={{fontSize:13}}/>
                <YAxis />
                <Tooltip />
                <Legend />
                <Line type="monotone" dataKey="Green" stroke={props.graphColors.green}  activeDot={{ r: 8 }} />
                <Line type="monotone" dataKey="Yellow" stroke={props.graphColors.yellow} />
                <Line type="monotone" dataKey="HighVolume" stroke={props.graphColors.hv} />
                <Line type="monotone" dataKey="LuxLimo" stroke={props.graphColors.lx} />
                <Line type="monotone" dataKey="Livery" stroke={props.graphColors.lv}/>
                <Line type="monotone" dataKey="BlackCar" stroke={props.graphColors.bc} />
            </LineChart> 
          </ResponsiveContainer>
          </div>
      )
  };
  
  export default graph;





/*
  class graph extends Component {
    render() {
    let my_data = this.props.data
        return (
            <div>
                <LineChart
                width={800}
                height={600}
                data={my_data}
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey={my_data.Green} activeDot={{ r: 8 }} />
      </LineChart> 
            </div>
        );
    }
}

export default graph;


**/