import React from 'react';
import './Slider.css';

const slider = (props) => {
    return(
        <div className="slidecontainer">
            <input 
            id="slider1" 
            type="range" 
            min="1" 
            max="200" 
            step="10" 
            //className="slider"
            onChange={props.handleSlider} />
        </div>
    )
};

export default slider;
